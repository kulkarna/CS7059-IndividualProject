package com.example.bollywoodhollywood;



/**
 * Created by LIMRA on 31-Oct-16.
 */

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class DBHandler extends SQLiteOpenHelper {

    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "MoviesInfo";

    // Contacts table name
    private static final String TABLE_Movies = "Movies";

    // Movies Table Columns names
    private static final String KEY_ID = "id";
    private static final String KEY_NAME = "name";
    private static final String KEY_ACTOR = "actor";
    private static final String KEY_Actress = "actress";
    private static final String KEY_YearOfRelease = "yearOfRelease";

    public DBHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        //context.deleteDatabase("MoviesInfo");
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_MOVIES_TABLE = "CREATE TABLE " + TABLE_Movies + "("
                + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL," + KEY_NAME + " TEXT,"+ KEY_ACTOR + " TEXT,"
                + KEY_Actress + " TEXT,"+ KEY_YearOfRelease + " TEXT" + ")";


        db.execSQL(CREATE_MOVIES_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_Movies);
        // Creating tables again
        onCreate(db);
    }

    // Adding new Movie
    public void addMovie(Movie movie) {

        SQLiteDatabase db = this.getWritableDatabase();
       // db.execSQL("delete from "+  TABLE_Movies);
        System.out.println("Adding Movie in DB at DB Handler");

        Log.d("dsfdf","addd");

        ContentValues values = new ContentValues();
        //values.put(KEY_ID,movie.getId());
        values.put(KEY_NAME,movie.getName());
        values.put(KEY_ACTOR, movie.getActor()); // Movie
        values.put(KEY_Actress, movie.getActress()); // Movie Actress
        values.put(KEY_YearOfRelease,movie.getYearOfRelease());
        // Inserting Row
        db.insert(TABLE_Movies, null, values);
        Log.d("dsfdf","closed and added");
        db.close(); // Closing database connection
    }

    // Getting one Movie
    public Movie getMovie(int id) {
       /* SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_Movies, new String[]{KEY_ID,
                        KEY_NAME, KEY_ACTOR,KEY_Actress,KEY_YearOfRelease}, KEY_ID + "=?",
                new String[]{String.valueOf(id)}, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();

        Movie contact = new Movie(Integer.parseInt(cursor.getString(0)),
                cursor.getString(1), cursor.getString(2), cursor.getString(3), cursor.getString(4));
        */
        // return Movie

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor =  db.rawQuery( "select * from Movies where id="+id+"", null );
        Movie movie = new Movie(Integer.parseInt(cursor.getString(0)),
                cursor.getString(1), cursor.getString(2), cursor.getString(3), cursor.getString(4));
        return movie;

    }

    // Getting All Movies
    public List<Movie> getAllMovies() {
        List<Movie> MovieList = new ArrayList<Movie>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_Movies;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Movie Movie = new Movie();
                Movie.setId(Integer.parseInt(cursor.getString(0)));
                Movie.setName(cursor.getString(1));
                Movie.setActor(cursor.getString(2));
                Movie.setActress(cursor.getString(3));
                Movie.setYearOfRelease(cursor.getString(4));
                // Adding contact to list
                MovieList.add(Movie);
            } while (cursor.moveToNext());
        }

        // return contact list
        return MovieList;
    }

    public String[] getAllMoviesName() {


        String[] MovieList = new String[getMoviesCount()];
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_Movies;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);


        Log.d("Movie namesdsd",cursor.toString());

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Movie Movie = new Movie();
                Movie.setId(Integer.parseInt(cursor.getString(0)));
                Movie.setName(cursor.getString(1));
                Movie.setActor(cursor.getString(2));
                Movie.setActress(cursor.getString(3));
                //Movie.setYearOfRelease(cursor.getString(4));
                // Adding contact to list

                MovieList[cursor.getPosition()]=Movie.getName();

            } while (cursor.moveToNext());
        }

        // return contact list
        return MovieList;
    }

    // Getting Movies Count
    public int getMoviesCount() {
        String countQuery = "SELECT  * FROM " + TABLE_Movies;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
       int count=cursor.getCount();
        cursor.close();

        // return count
        return count;
    }

    // Updating a Movie
    public int updateMovie(Movie Movie) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(KEY_NAME, Movie.getName());
        values.put(KEY_ACTOR, Movie.getActor());
        values.put(KEY_Actress, Movie.getActress());
        values.put(KEY_YearOfRelease, Movie.getYearOfRelease());

        // updating row
        return db.update(TABLE_Movies, values, KEY_ID + " = ?",
                new String[]{String.valueOf(Movie.getId())});
    }

    // Deleting a Movie
    public void deleteMovie(Movie Movie) {
        SQLiteDatabase db = this.getWritableDatabase();
        //db.delete(TABLE_Movies, KEY_ID + " = ?",
                //new String[] { String.valueOf(Movie.getId()) });

        db.execSQL("delete from "+ TABLE_Movies);
        db.close();
    }


}

