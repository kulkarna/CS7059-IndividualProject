package com.example.bollywoodhollywood;

import android.app.Activity;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;
import java.util.Random;

public class PrimaryActivity extends Activity  {
    CurrentGameStatus current_game;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_primary);
        Intent intent = getIntent();
        String message = MainActivity.EXTRA_MESSAGE;

        System.out.println("Adding Movie in DB");
        DBHandler db = new DBHandler(this);


        if(message.equalsIgnoreCase("Bollywood")) {
            //Log.d("dsfdf","addd");
            //System.out.println(db.getMoviesCount());
            db.deleteMovie(new Movie("ROCKON", "Ranbir Kapoor", "Anushka Sharma", "2016"));
            System.out.println("Bollywood");
            db.addMovie(new Movie("ROCKON2", "FARHAN AKHTAR", "SHRADDHA KAPOOR", "2016"));
            db.addMovie(new Movie("SHOLAY", "Dharmendra", "Hema Malini", "1975"));
            db.addMovie(new Movie("3Idiots", "Aamir Khan", "Kareena Kapoor", "2009"));
            db.addMovie(new Movie("DHADKAN", "SUNIEL SHETTY", "SHILPA SHETTY", "1998"));
            db.addMovie(new Movie("Lagaan", "Aamir Khan", "Gracy Singh", "2001"));
            db.addMovie(new Movie("FIZA", "HRITIK ROSHAN", "KAREENA KAPOOR", "2005"));
            db.addMovie(new Movie("MotherIndia", "Sunil Dutt", "Nargis", "1957"));
            db.addMovie(new Movie("Anand", "Rajesh Khanna", "Sumita Sanyal", "1971"));
            db.addMovie(new Movie("AndazApnaApna", "Salman Khan", "Karishma Kapoor", "1994"));
            db.addMovie(new Movie("Devdas", "Shah Rukh Khan", "Aishwarya Rai", "2002"));
            db.addMovie(new Movie("Golmaal", "Amol Palekar", "Bindiya Goswami", "1979"));
            db.addMovie(new Movie("Swades", "Shah Rukh Khan", "Gayatri Joshi", "2004"));
            db.addMovie(new Movie("MunnabhaiMBBS", "Sanjay Dutt", "Gracy Singh", "2003"));
            db.addMovie(new Movie("Guide", "Dev Anand", "Waheeda Rehman", "1965"));
            db.addMovie(new Movie("HeraPheri", "Akshay Kumar", "Tabu", "2000"));
            db.addMovie(new Movie("RangDeBasanti", "Aamir Khan", "Soha Ali Khan", "2006"));
            db.addMovie(new Movie("ChakDeIndia", "Shah Rukh Khan", "Sagarika Ghatge", "2007"));
            db.addMovie(new Movie("JabWeMet", "Shahid Kapoor", "KAREENA KAPOOR", "2007"));
            db.addMovie(new Movie("TareZameenPar", "Darsheel Safary", "Tisca Chopra", "2007"));
            db.addMovie(new Movie("Pyasa", "Guru Dutt", "Waheeda Rehman", "1957"));
            db.addMovie(new Movie("Deewar", "Shashi Kapoor", "Neetu Singh", "1979"));
            db.addMovie(new Movie("Awara", "Raj Kapoor", "Nargis", "1956"));
            db.addMovie(new Movie("RangDeBasanti", "Aamir Khan", "Soha Ali Khan", "2006"));
            db.addMovie(new Movie("MrIndia", "Anil Kapoor", "Sridevi", "1987"));
            db.addMovie(new Movie("Satya", "Manoj Bajpayee", "Urmila Matondkar", "1998"));
            db.addMovie(new Movie("Barfi", "Ranbir Kapoor", "Priyanka Kapoor", "2012"));
            db.addMovie(new Movie("DilSe", "Shah Rukh Khan", "Manisha Koirala", "1998"));
            db.addMovie(new Movie("Queen", "Raj Kumar Yadav", "Kangana Ranaut", "2013"));
            db.addMovie(new Movie("VeerZara", "Shah Rukh Khan", "Priety Zinta", "2004"));
            db.addMovie(new Movie("Masoom", "Jugal Hansraj", "Urmila Matondkar", "1983"));
            db.addMovie(new Movie("TheLunchbox", "Irfan Khan", "Nimrat Kaur", "2013"));
            db.addMovie(new Movie("Airlift", "Akshay Kumar", "Nimrat Kaur", "2016"));
            db.addMovie(new Movie("Udaan", "Ronit Roy", "NA", "2010"));
            db.addMovie(new Movie("Pakeezah", "Raaj Kumar", "Meena Kumari", "1972"));
            db.addMovie(new Movie("SalaamBombay", "Nana Patekar", "Anita Kanwar", "1988"));
            db.addMovie(new Movie("Bombay", "Arvind Swamy", "Manisha Koirala", "1994"));
            db.addMovie(new Movie("Befikre", "Ranveer Singh", "Vaani Kapoor", "2016"));
            db.addMovie(new Movie("DearZindagi", "Shah Rukh Khan", "Aalia Bhatt", "2016"));
            db.addMovie(new Movie("Force2", "John Abraham", "Sonakshi Sinha", "2016"));
            db.addMovie(new Movie("Dangal", "Aamir Khan", "Sakshi Tanwar", "2016"));
            db.addMovie(new Movie("Sultan", "Salman Khan", "Anushka Sharma", "2016"));
            db.addMovie(new Movie("Shivaay", "Ajay Devgan", "Sayesha Saigal", "2016"));
            db.addMovie(new Movie("Mirziya", "Harshavardhan Kapoor", "Saiyami Kher", "2016"));
            db.addMovie(new Movie("Dishoom", "John Abraham", "Jacqueline Fernandez", "2016"));
            db.addMovie(new Movie("Kick", "Salman Khan", "Jacqueline Fernandez", "2014"));
            db.addMovie(new Movie("MohenJoDaro", "Hrithik Roshan", "Pooja Hegde", "2016"));
            db.addMovie(new Movie("Banjo", "Riteish Deshmukh", "Nargis Fakhri", "2016"));
            db.addMovie(new Movie("Neerja", "Shekhar Ravjiani", "Sonam Kapoor", "2016"));
            db.addMovie(new Movie("UdtaPunjab", "Shahid Kapoor", "Aalia Bhatt", "2016"));
            db.addMovie(new Movie("Pink", "Amitabh Bachchan", "Tapsee Pannu", "2016"));


        }
        else if(message.equalsIgnoreCase("Hollywood")){
            db.deleteMovie(new Movie("ROCKY","Sylvester Stallone","None","1976"));
            System.out.println("Hollywood");
            db.addMovie(new Movie("Rocky","Sylvester Stallone","None","1976"));
            db.addMovie(new Movie("Goodfellas","Robert De Niro","Lorraine Bracco","1990"));
            db.addMovie(new Movie("Titanic","Leonardo Di Caprio","Kate Winslet","1997"));
            db.addMovie(new Movie("Casablanca","Humphrey Bogart","Ingrid Bergman","1942"));
            db.addMovie(new Movie("Inception","Leonardo Di Caprio","Ellen Page","2010"));
            db.addMovie(new Movie("RagingBull","Robert De Niro","Cathy Moriarty","1981"));
            db.addMovie(new Movie("TaxiDriver","Robert De Niro","Jodie Foster","1976"));
            db.addMovie(new Movie("ApocalypseNow","Marlon Brando","None","1979"));
            db.addMovie(new Movie("SchindlersList","Liam Neeson","Embeth Davidtz","1993"));
            db.addMovie(new Movie("Psycho","Anthony Perkins","Janet Leigh","1961"));
            db.addMovie(new Movie("ForestGump","Tom Hanks","Sally Field","1994"));
            db.addMovie(new Movie("Vertigo","James Stewart","Kim Novak","1958"));
            db.addMovie(new Movie("TheGodfather","Al Pacino","Talia Shire","1972"));
            db.addMovie(new Movie("Gladiator","Russell Crowe","Connie Nielson","2000"));
            db.addMovie(new Movie("TheMatrix","Keanu Reeves","Carrie-Anne Moss","1999"));
            db.addMovie(new Movie("Avatar","Sam Worthington","Zoe Saldana","2009"));
            db.addMovie(new Movie("IronMan","Robert Downey Jr.","Gwyneth Paltrow","2008"));
            db.addMovie(new Movie("Gravity","George Clooney","Sandra Bullock","2013"));
            db.addMovie(new Movie("Interstellar","Matthew McConaughey","Anne Hathway","2014"));
            db.addMovie(new Movie("TheAvengers","Robert Downey Jr","Scarlett Johansson","2012"));
            db.addMovie(new Movie("JurassicPark","Jeff Goldblum","Laura Dern","1993"));
            db.addMovie(new Movie("JurassicWorld","Chris Pratt","Bryce Dallas Howard","2015"));
            db.addMovie(new Movie("IndependenceDay","Will Smith","Vivica Fox","1996"));
            db.addMovie(new Movie("ManOfSteel","Henry Cavill","Amy Adams","2013"));
            db.addMovie(new Movie("StarTrek","Chris Pine","Zoe Saldana","2009"));
            db.addMovie(new Movie("Transformers","OptimusPrime","NA","2007"));
            db.addMovie(new Movie("StarWars","Mark Hamill","Carrie Fisher","1979"));
            db.addMovie(new Movie("Skyfall","Daniel Craig","Bérénice Marlohe","2012"));
            db.addMovie(new Movie("FightClub","Brad Pitt","Helena Bonham Carter","1999"));
            db.addMovie(new Movie("CitizenKane","Orson Welles","Ruth Warrick","1941"));
            db.addMovie(new Movie("LawrenceOfArabia","Peter O'Toole","NA","1963"));
            db.addMovie(new Movie("PulpFiction","John Travolta","Uma Thurman","1994"));
            db.addMovie(new Movie("TheShawshankRedemption","Tim Robbins","NA","1994"));
            db.addMovie(new Movie("Deadpool","Ryan Reynolds","Morena Baccarin","2016"));
            db.addMovie(new Movie("Furious7","Vin Diesel","Michelle Rodriguez","2015"));
            db.addMovie(new Movie("BatmanBegins","Christian Bale","Katie Holmes","2005"));
            db.addMovie(new Movie("Braveheart","Mel Gibson","Sophie Marceau","1995"));
            db.addMovie(new Movie("LoveActually","Hugh Grant","Emma Thompson","2003"));
            db.addMovie(new Movie("TheNotebook","Ryan Gosling","Rachel McAdams","2004"));
            db.addMovie(new Movie("PrettyWoman","Richard Gere","Julia Roberts","1990"));
            db.addMovie(new Movie("NottingHill","Hugh Grant","Julia Roberts","1999"));
            db.addMovie(new Movie("AnnieHall","Woody Allen","Diane Keaton","1977"));
            db.addMovie(new Movie("TheProposal","Ryan Reynolds","Sandra Bullock","2009"));
            db.addMovie(new Movie("ThePrincessBride","Mandy Patinkin","Cary Elwes","1987"));
            db.addMovie(new Movie("BeforeSunrise","Ethan Hawke","Julie Delpy","1995"));
            db.addMovie(new Movie("PSILoveYou","Gerard Butler","Hilary Swank","2007"));
            db.addMovie(new Movie("Hitch","Will Smith","Eva Mendes","2005"));
            db.addMovie(new Movie("TheHoliday","Jude Law","Cameron Diaz","2006"));


        }
        System.out.println(db.getMoviesCount());
        List<Movie> movieList = db.getAllMovies();


        current_game=new CurrentGameStatus(movieList);

        TextView wordView = (TextView) findViewById(R.id.word);
        TextView scoreText = (TextView) findViewById(R.id.textViewScore);



        System.out.println("Game Created with Word: " + current_game.get_raw_word());
        System.out.println("Game Created with Score: " + current_game.get_score());
        System.out.println("Game Created with Score: " + current_game.get_display_word());

        wordView.setText(current_game.get_display_word());
        //Log.d("Movie name",db.getAllMoviesName()[0]);
        scoreText.setText(Integer.toString(current_game.get_score()));

    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_screen, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    public void new_guess(View view) {

        TextView wordView = (TextView) findViewById(R.id.word);
        ProgressBar progressbar = (ProgressBar) findViewById(R.id.progressBar);
        TextView scoreText = (TextView) findViewById(R.id.textViewScore);
        TextView letter_guessed = (TextView) findViewById(R.id.new_letter);
        // TextView wordColor=(TextView)findViewById(R.id.wordHolly);

        String new_guess = letter_guessed.getText().toString();

        int color = 0xFF00FF00;
        //progressbar.setBackgroundColor(color);
        progressbar.setDrawingCacheBackgroundColor(color);


        if (!new_guess.equals("")) {

            boolean letterStatus=current_game.try_to_insert_letter(new_guess);
            wordView.setText(current_game.get_display_word());
            int score=current_game.get_score();
            scoreText.setText(Integer.toString(score));

            if(letterStatus){
                Toast.makeText(getApplicationContext(), current_game.correctLetter, Toast.LENGTH_SHORT).show();
            }
            else {
                Toast.makeText(getApplicationContext(), current_game.wrongLetter, Toast.LENGTH_SHORT).show();
            }

            if(score>=1 && score<=2){

                //wordColor.setTextColor(Color.parseColor("#ff0000"));
                scoreText.setTextColor(Color.parseColor("#ff0000"));

                // Toast.makeText(getApplicationContext(), "Last Chance left Good Luck..!!", Toast.LENGTH_LONG).show();

            }
            else if(score>=3 && score<=7){

                //wordColor.setTextColor(Color.parseColor("#ff0000"));
                scoreText.setTextColor(Color.parseColor("#ffa500"));

                // Toast.makeText(getApplicationContext(), "Last Chance left Good Luck..!!", Toast.LENGTH_LONG).show();

            }
            else if(score>=8 && score<=10){

                //wordColor.setTextColor(Color.parseColor("#ff0000"));
                scoreText.setTextColor(Color.parseColor("#006400"));

                // Toast.makeText(getApplicationContext(), "Last Chance left Good Luck..!!", Toast.LENGTH_LONG).show();

            }
            else if(score==0){
                Toast.makeText(getApplicationContext(), "GAME OVER..!!", Toast.LENGTH_SHORT).show();
                // scoreText.setText(Integer.toString(100));
                //reset(view);
                wordView.setTextColor(Color.parseColor("#ffa500"));


                wordView.setText(current_game.get_display_raw_word());

            }
            scoreText.setText(Integer.toString(score));
            letter_guessed.setText("");


            if (current_game.word_completed()) {
                Toast.makeText(getApplicationContext(), "Congrats!!!! \n You guessed " + current_game.get_raw_word().toUpperCase() + " correctly", Toast.LENGTH_LONG).show();
                wordView.setTextColor(Color.parseColor("#00ff00"));
                //scoreText.setText(Integer.toString(100));
                //reset(view);
            }
        }
    }

    public void Hint(View view){

        Toast.makeText(getApplicationContext(), "Name of Actor: "+current_game.actor+"\n"+"Name of Actress: "+current_game.actress+"\n"+"Year Of Release: "+current_game.yearOfRelease,Toast.LENGTH_LONG).show();
    }

    public void reset(View view) {
        DBHandler db=new DBHandler(this);
        List<Movie> movieList=db.getAllMovies();
        current_game=new CurrentGameStatus(movieList);

        //TextView wordColor=(TextView)findViewById(R.id.wordHolly);
        //wordColor.setTextColor(Color.parseColor("#0099cc"));

        TextView wordView = (TextView) findViewById(R.id.word);
        wordView.setText(current_game.get_display_word());
        wordView.setTextColor(Color.parseColor("#FFFFFF"));

        TextView scoreText = (TextView) findViewById(R.id.textViewScore);
        scoreText.setTextColor(Color.parseColor("#006400"));
        scoreText.setText(Integer.toString(10));

        System.out.println("In reset function");
    }

}
