package com.example.bollywoodhollywood;

import android.content.Context;
import android.content.ContextWrapper;
import android.util.Log;

import java.util.List;
import java.util.Random;

public class CurrentGameStatus {

    static Score score;
    static WordToGuess wordToGuess;
    public static String actor;
    public static String actress;
    public static String yearOfRelease;
    public static String wrongLetter;
    public static String correctLetter;
    CurrentGameStatus(List<Movie> allMovieList){

        score = new Score();
        Random randomGenerator = new Random();
        int randomNo=randomGenerator.nextInt(allMovieList.size());
        Movie movie=allMovieList.get(randomNo);
        wordToGuess = new WordToGuess(movie.getName());
        actor=movie.getActor();
        actress=movie.getActress();
        yearOfRelease=movie.getYearOfRelease();
    }




    int get_score(){
        return score.return_score();
    }

    String get_raw_word(){
        return wordToGuess.return_raw_word();
    }

    String get_display_word(){
        return wordToGuess.display_word();
    }

    String get_display_raw_word(){
        return wordToGuess.display_raw_word();
    }
   /* String get_display_word(String movieName){
        return wordToGuess.display_word(movieName);
    }*/

   public boolean try_to_insert_letter(String guessed_letter) {

        if (wordToGuess.letter_belongs_to_word(guessed_letter) && !wordToGuess.letter_already_in_word(guessed_letter)) {
            wordToGuess.insert_letter(guessed_letter);
            System.out.println("New Guess is:"+guessed_letter);
            correctLetter="Letter matched..!!";
            return true;
            //score.update_score_correct_letter();

        } else {
            wrongLetter="Letter does not match..!!";
            score.update_score_incorrect_letter();
            return false;
        }
    }

    boolean word_completed(){
        return wordToGuess.word_completed();
    }
}
