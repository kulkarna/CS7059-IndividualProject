package com.example.bollywoodhollywood;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;
import java.util.Random;


public class MainActivity extends Activity {


    public static String EXTRA_MESSAGE = "com.example.myfirstapp.MESSAGE";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_screen);
        Intent intent = getIntent();
    }

    /** Called when the user clicks the button */
    public void bollywood(View view) {
        Intent intent = new Intent(this, PrimaryActivity.class);
        EXTRA_MESSAGE = "Bollywood";
        startActivity(intent);
    }

    public void hollywood(View view) {
        Intent intent = new Intent(this, PrimaryActivity.class);
        EXTRA_MESSAGE = "Hollywood";
        startActivity(intent);
    }


}

