package com.example.bollywoodhollywood;

/**
 * Created by LIMRA on 31-Oct-16.
 */

public class Movie {
    private int id;
    private String name;
    private String actor;
    private String actress;
    private String yearOfRelease;

    public Movie()
    {
    }

    public Movie(int id,String name,String actor,String actress,String yearOfRelease)
    {
        this.id=id;
        this.name=name;
        this.actor=actor;
        this.actress=actress;
        this.yearOfRelease=yearOfRelease;
    }



    public Movie(String name,String actor, String actress, String yearOfRelease)
    {
        this.actor=actor;
        this.actress=actress;
        this.name=name;
        this.yearOfRelease=yearOfRelease;
    }

    public void setId(int id) {
        this.id = id;
    }
    public int getId() {

        return id;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public void setActor(String actor) {
        this.actor=actor;
    }

    public void setActress(String actress) {
        this.actress = actress;
    }


    public void setYearOfRelease(String yearOfRelease) {
        this.yearOfRelease = yearOfRelease;
    }

    public String getActor() {
        return actor;
    }

    public String getActress() {
        return actress;
    }

    public String getYearOfRelease() {
        return yearOfRelease;
    }


}
