package com.example.bollywoodhollywood;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class AboutActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
    }

    /** Called when the user clicks the button */
    public void OK(View view) {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }
}
